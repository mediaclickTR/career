<?php


Route::group(['prefix' => 'Career', 'as' => 'Career.'], function () {

    Route::get('/list/{sitemap_id}',['as'=> 'list.index','uses'=>'CareerController@index']);
});
