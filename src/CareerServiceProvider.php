<?php

namespace Mediapress\Career;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Route;

class CareerServiceProvider extends ServiceProvider
{
    public $namespace = 'Mediapress\Career';

    public const HELPERS_PHP = 'helpers.php';


    public function boot()
    {
        $this->map();


        if (file_exists(__DIR__.DIRECTORY_SEPARATOR.'Http'.DIRECTORY_SEPARATOR.self::HELPERS_PHP)) {
            include_once __DIR__.DIRECTORY_SEPARATOR.'Http'.DIRECTORY_SEPARATOR.self::HELPERS_PHP;
        }
        $this->loadTranslationsFrom(__DIR__.DIRECTORY_SEPARATOR.'Resources'.DIRECTORY_SEPARATOR.'lang', 'Career');
    }

    public function register()
    {
        //
    }

    private function map()
    {
        $this->mapPanelRoutes();
    }

    protected function mapPanelRoutes()
    {
        $routes_file = __DIR__.DIRECTORY_SEPARATOR.'Routes'.DIRECTORY_SEPARATOR.'PanelRoutes.php';
        Route::group([
            'middleware' => 'web',
            'namespace' => $this->namespace.'\Http\Controllers',
            'prefix' => 'mp-admin',
        ], function ($router) use ($routes_file) {
            if (is_file($routes_file)) {
                include_once $routes_file;
            }
        });
    }


}


