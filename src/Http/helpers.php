<?php


function career_button($sitemap)
{
    return ' <a href="'.route("Career.list.index",$sitemap->id).'"
                       class="filter-block btn-warning btn-sm btn"
                       title="'.trans("ContentPanel::sitemap.edit_properties").'">
                        <i class="fa fa-users"></i>'.trans("Career::career.sitemap.button.name").'
                    </a>';

}
